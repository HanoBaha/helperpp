# helPerl
A Perl script to help you with file generation.

Currently it only (barely) handle hpp to cpp generation.
At some point, it will be able to generate hpp skeleton, as well as HTML, Latek, etc.

## Installation
1. Install Perl
2. clone the repo
3. you're good to go

## Usage
`./helPerl hpp_file` : will generate a cpp file from given hpp file.
