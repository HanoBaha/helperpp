#!/usr/bin/env perl
#!/usr/bin/perl
use strict;
use warnings;

# Parsing argument
# ================

my $hpp_path = $ARGV[0];
my $hpp_filename = "";
{
    my @tmp = split('/', $hpp_path);
    $hpp_filename = $tmp[-1];
}
my $cpp_file = $hpp_path =~ s/\.hpp/.cpp/gr;

# Gestion fichiers
# ================
open(my $in, "<", "$hpp_path") or die "Couldn't open $hpp_path";
open(my $out, ">", "$cpp_file") or die "Couldn't create $cpp_file";

print $out "#include \"$hpp_filename\"\n\n";

# Regex
# =====
my $keyword = qr/(static|mutable|inline|constexpr|consteval|constinit)/;

my $type = qr/(const\s)?(unsigned\s)?(\w|\:\:|<.*>)+(\sconst)?(\s?(\*|&)(\s?const\s)?)*/;

# Déclaration de variable totale :
# (?<keyword>$keyword+\s)*(?<type>$type)(?<extra_space>\s?)(?<name>\w+)
# | static, mutable etc. || type \s?   || espace en plus  || nom      |
my $argument_list = qr/($type\s?\w+)(,\s$type\s?\w+)*/;

my $func_regex = qr/(?<keyword>$keyword\s)*(?<type>$type)\s?(?<name>\w+)\s?\((?<arguments>($argument_list|))\)\s*(?<final_const>(const|))(\soverride)?;/;
#                    static constexpr       unsigned int     function       (  int& x, int& y              )   const                     override;

my $class_name = "";
# TODO : Avoir un tableau de namespace qui retient combien de classe/struct ont été imbriquée
# class Todo { struct Foo { void f(); };};
# devrait générer 
# void Todo::Foo::f() {}
#
# Il faudrait pouvoir pop les namespaces quand on sort de la définition des class/struct
# À priori, seul les class/struct finissent par };
# Ça pourrait être notre indicateur de pop.
# Toutefois il est possible de trouver des }; dans certains cas.
# Si l'utilisateurs à placer un ; par habitude
# Ou si des choses compliqué sont définit dans le header.
#
# Pour vérifier que tout se passe bien, on peut compter le nombre de namespace différent total
# Et le nombre de }; trouvé.
# S'il n'y en a pas le même nombre, alors il y a un problème et on peut l'indiquer à l'utilisateur.
my $namespace = "";
my @function_list = ();
my @static_variable = ();

# Parsing fichier
# ===============
while(<$in>)
{
    s/ +/ /g; # Get rid of extra space
    
    # Get class name
    if ( /^(class|struct)\s(\w+)/ ) {
        $class_name = "$2";
        $namespace = "$class_name\:\:";
    }
    # Skip inline function
    elsif ( /inline/ ) {
        next;
    }
    # Constructor / Destructor
    elsif ( /(?<tilde>~)?$class_name\s*\((?<args>$argument_list*)\);/ ) {
        my $const_dest = "$namespace$+{tilde}$class_name($+{args})\n{\n\n}\n\n";
        push(@function_list, $const_dest);
    }
    # Is function
    elsif ( /$func_regex/ ) {
        my $func_impl = "$+{type} $namespace$+{name}($+{arguments}) $+{final_const}\n";
        $func_impl .= "{\n";
        $func_impl .= "\n";
        if ($+{type} ne 'void') {
            $func_impl .= "\treturn {};\n";
        }
        $func_impl .= "}\n\n";
        push(@function_list, $func_impl);
    }
    # Static variable
    # Static constexpr have = symbol and a value, so it won't be capture here.
    elsif ( /static\s(?<keyword>$keyword+\s)*(?<type>$type)(?<extra_space>\s?)(?<name>\w+);/ ) {
        my $var = "$+{type}$+{extra_space}$namespace$+{name} = {};\n";
        push(@static_variable, $var);
    }
}

# Génération du cpp
# =================
foreach (@static_variable) {
    print $out "$_";
}

print $out "\n";

foreach (@function_list) {
    $_ =~ s/static //g; # Remove static keyword
    print $out "$_";
}

close $in or die "$in : $!";
