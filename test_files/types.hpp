#include <array>
#include <unordered_map>

class Var
{
public:
    const int* something_wrong_I_can_feel_it;
    static int basic;
    static unsigned int unsigned_type;
    static const int Constvar;
    static const int *Constvar_ptr;
    static const int* Constvar_ptr_space;
    static int const varconst;
    static int const *varconst_ptr;
    static int const* varconst_ptr_space;
    static int* pointer;
    static int *space_pointer;
    static int& reference;
    static int &space_reference;
    static int*** pointers;
    static int*& pointers_and_refs;
    static int**& more_pointers_and_refs;
    static const int * const Const_pointer;
    static int * const * * const Const_pointers;
    static std::array<unsigned int, 5> stl_type;
    static std::array<std::array<int **const, 48>, 2>& more_stl_type;
    static std::unordered_map<int, std::array<int *, 48>> morer_stl_type;
    static const unsigned int *&a_lot_at_once;
};
