#include "functions.hpp"


Foo::Foo()
{

}

Foo::Foo(int hello, bool hi)
{

}

Foo::~Foo()
{

}

void Foo::simple() 
{

}

void Foo::Const_func() const
{

}

void Foo::override_func() 
{

}

void Foo::Const_and_override() const
{

}

void Foo::static_func() 
{

}

void Foo::arguments(int hello) 
{

}

void Foo::more_arguments(bool never, int gonna, float let, double you, unsigned char dooooooown) const
{

}

int* Foo::some_fancy_types(std::unordered_map<int, std::array<int *, 48>> ooof, const int * const please_welp) 
{

	return {};
}

