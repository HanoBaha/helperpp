#include "types.hpp"

int Var::basic = {};
unsigned int Var::unsigned_type = {};
const int Var::Constvar = {};
const int *Var::Constvar_ptr = {};
const int* Var::Constvar_ptr_space = {};
int const Var::varconst = {};
int const *Var::varconst_ptr = {};
int const* Var::varconst_ptr_space = {};
int* Var::pointer = {};
int *Var::space_pointer = {};
int& Var::reference = {};
int &Var::space_reference = {};
int*** Var::pointers = {};
int*& Var::pointers_and_refs = {};
int**& Var::more_pointers_and_refs = {};
const int * const Var::Const_pointer = {};
int * const * * const Var::Const_pointers = {};
std::array<unsigned int, 5> Var::stl_type = {};
std::array<std::array<int **const, 48>, 2>& Var::more_stl_type = {};
std::unordered_map<int, std::array<int *, 48>> Var::morer_stl_type = {};
const unsigned int *&Var::a_lot_at_once = {};

