#ifndef __FOO__
#define __FOO__

#include <unordered_map>
#include <array>
#include <string>

class Foo
{
public:
    Foo();
    Foo(int hello, bool hi);
    Foo(const Foo& ahah_heres_a_trick) = default;
    ~Foo();

    void simple();
    void Const_func() const;
    void override_func() override;
    void Const_and_override() const override;
    static void static_func();
    inline void shouldnt_be_there();
    static inline void never_gonna_give_you_up();
    void arguments(int hello);
    void more_arguments(bool never, int gonna, float let, double you, unsigned char dooooooown) const;
    int* some_fancy_types(std::unordered_map<int, std::array<int *, 48>> ooof, const int * const please_welp);
};

#endif
